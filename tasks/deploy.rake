namespace :macro do |ns|

  desc "Deploy to live mingle instance"
  task :deploy do
    macro_name = "globalhand_linkbar_macro"
    github_url = "git@bitbucket.org:crossroadsIT/globalhand_linkbar_macro.git"
    macro_path = "/opt/mingle/current/vendor/plugins/#{macro_name}"

    machine = "mingle.crossroadsint.org"
    user = "root"

    command = "if [ -d #{macro_path} ]; \
then cd #{macro_path} && git pull origin master; \
else git clone #{github_url} #{macro_path}; \
fi; /etc/init.d/mingle restart; exit"

    system("ssh #{user}@#{machine} '#{command}'")

    puts "#{macro_name} successfully deployed to #{machine}."
  end

end
