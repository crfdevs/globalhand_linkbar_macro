class GhLinkbarMacro
  def initialize(params, project, current_user)
    @params = params
    @project = project
  end

  def execute
    validate_params
    conform_params

    require "erb"
    template = File.open(File.join(File.dirname(__FILE__), "..", "views", "toolbar.html.erb")).read
    ERB.new(template, 0, "<%=%>").result(binding)
  end

  def can_be_cached?
    true
  end

  def project
    @project.identifier
  end

  def conform_params
    # Split params into their heirarchy levels.
    # If a param is an array, expand it into a conformed hash.
    @param_levels = @params["heirarchy"].map do |level|
      if @params[level]["values"].is_a?(Array)
        # Turns ['label'] into {'label' => {:value => 'label'})
        @params[level]["values"].inject({}){|r,n| r[n] = {:value => n}; r }
      else
        @params[level]["values"]
      end
    end
  end

  def url_modifiers
    @url_modifiers ||= @params["heirarchy"].map{|level| @params[level]["url_modifier"] }
  end

  def build_link_url(url_template, url_modifiers)
    # Substitute project
    url = url_template.dup.gsub(":project", project)
    # Substitute url modifiers
    url_modifiers.each do |url_params, data|
      data.each {|k, v| url_params = url_params.gsub(":#{k}", v.to_s) }
      url << "&" << url_params
    end
    url
  end

  def link_to(text, url, title="")
    title = " title=\"#{title}\"" if title != ""
    "<a href=\"#{url}\"#{title}>#{text}</a>"
  end

  def validate_params
    raise "No heirarchy specified."           unless @params["heirarchy"]
    @params["heirarchy"].each do |level|
      raise "No #{level} specified."          unless @params[level]
    end
    raise "Please specify url_template." unless @params["url_template"]
  end
end

