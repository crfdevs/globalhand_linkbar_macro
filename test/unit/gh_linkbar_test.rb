require File.join(File.dirname(__FILE__), 'unit_test_helper')

module GhLinkbarHelper
  def sample_params
    @sample_params ||= YAML.load %Q{
url_template: "/projects/:project/cards/new?properties[status]=open"
heirarchy:
  - teams
  - card_types
  - epics
card_types:
  url_modifier: "properties[Type]=:value"
  values:
    - "Defect"
    - "Deliverable"
teams:
  url_modifier: "properties[Team]=:value"
  values:
    - "Apple"
    - "Orange"
epics:
  url_modifier: "properties[Name]=:code - &properties[Epic]=:id"
  values:
    "Global Hand":
      code: GH
      id:   532
    "UN Interface":
      code: UN
      id:   531
    "Stockit":
      code: STK
      id: 3875
    "Fat Free CRM":
      code: CRM
      id:   4062
    "Clockit":
      code: VOL
      id:   4057
    "Global Handicrafts Store":
      code: GHS
      id:   4058
extra_modifiers:
  "Test Modifier": "properties[testType]=(TEST)"
      }
  end
end

class GhLinkbarMacroTest < Test::Unit::TestCase
  include GhLinkbarHelper

  FIXTURE = 'sample'

  def test_gh_linkbar
    result = GhLinkbarMacro.new(sample_params, project(FIXTURE), nil).execute
    assert result.include?("title=\"Create a Deliverable.")
    assert result.include?("Fat Free CRM")
    assert result.include?("/projects/gh/cards/new?")

    assert result.include?("url_modifier_0")
    assert result.include?("Test Modifier</label>")
  end

  def test_gh_linkbar_params_validation
    params = {}
    assert_raise(RuntimeError) { GhLinkbarMacro.new(params, project(FIXTURE), nil).execute }
  end

  def test_gh_linkbar_link_building
    macro = GhLinkbarMacro.new(sample_params, project(FIXTURE), nil)
    url_modifiers = {"properties[Type]=:value" => {:value => "TestType"}}
    result = macro.build_link_url(sample_params["url_template"], url_modifiers)
    assert result.include?("properties[Type]=TestType")
  end

end

